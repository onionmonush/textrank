package com.radishfactory.textrank;

import java.util.Map;
import java.util.Set;
import java.util.List;
import java.util.HashSet;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.stream.Collectors;

import com.google.common.collect.Table;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableMap;

// Repeat until converges:
//
// WS(Vi) = (1 - d) + d * sum over Vj in IN(VI) {
//		w(i,j) * WS(Vj)
//		---------------
//		sum over Vk in OUT(vj) {
//			w(j,k)
//		}
// }
// todo table stuff
public class TextRanker {
		private double dampingConstant;
		private double convergenceLimit;
		private int maxIterations;

		public TextRanker(double dampingConstant, double convergenceLimit, int maxIterations) {
			this.dampingConstant = dampingConstant;
			this.convergenceLimit = convergenceLimit;
			this.maxIterations = maxIterations;
		}

		public List<String> getSummaryByChunks(String textToSummarize, int numChunks) {
			Document parsedDocument = new Document(textToSummarize);
			SentanceSimilarities sentanceSimilarities =
					new SentanceSimilarities(parsedDocument.getSentances());
			ImmutableMap<Sentance, Double> sentanceRanks = buildSentanceRanks(sentanceSimilarities);

			List<Sentance> summary = new ArrayList<Sentance>();
			int sentancesForChunk = parsedDocument.numSentances() / numChunks;
			for(int i = 0; i < numChunks - 1; i++) {
				summary.add(highestScoringSentance(
					parsedDocument.getSentances()
						.subList(i * sentancesForChunk, (i + 1) * sentancesForChunk),
					sentanceRanks));
			}
			// in case numSentances does not fit into chunk size evenly
			summary.add(highestScoringSentance(
					parsedDocument.getSentances()
						.subList((numChunks - 1) * sentancesForChunk, parsedDocument.numSentances()),
					sentanceRanks));
			return summary.stream()
				.map(sentance -> sentance.getSentanceText())
				.collect(Collectors.toList());
		}

		private Sentance highestScoringSentance(List<Sentance> sentances,
				ImmutableMap<Sentance, Double> sentanceScores) {

				Sentance highestScoringSentance = sentances.get(0);
				double currentHighestScore = 0;
				for(Sentance sentance: sentances) {
					if(sentanceScores.get(sentance) > currentHighestScore) {
						currentHighestScore = sentanceScores.get(sentance);
						highestScoringSentance = sentance;
					}
				}
				return highestScoringSentance;
		}



		private ImmutableMap<Sentance, Double> buildSentanceRanks(SentanceSimilarities sentanceSimilarities) {
			HashMap<Sentance, Double> previousSentanceRank = new HashMap<Sentance, Double>();
			HashMap<Sentance, Double> thisSentanceRank = new HashMap<Sentance, Double>();

			Table<Sentance, Sentance, Double> similarityTable = sentanceSimilarities.getSentanceSimilarityTable();
			Map<Sentance, Double> importanceOutflows = sentanceSimilarities.getSentanceImportanceOutflows();
			Set<Sentance> allSentances = similarityTable.columnKeySet(); 

			for(Sentance sentance : allSentances) {
				previousSentanceRank.put(sentance, 1.); 
				thisSentanceRank.put(sentance, 1.);
			}
			
			double minIterationDifference = Double.MAX_VALUE;
			int iteration = 0;
			while (iteration < 2 || (minIterationDifference > convergenceLimit && iteration < maxIterations)) {
				for(Sentance sentance : allSentances) {
					previousSentanceRank.put(sentance, thisSentanceRank.get(sentance));

					double nextImportanceNumerator = 0;
					double nextImportanceDenomenator = 0;

					for(Sentance otherSentance : allSentances) {
						if (otherSentance == sentance) {
							continue;
						}
						nextImportanceNumerator += similarityTable.get(sentance, otherSentance) 
								* thisSentanceRank.get(sentance);
						nextImportanceDenomenator += importanceOutflows.get(otherSentance);
					}
					thisSentanceRank.put(sentance, nextImportanceNumerator / nextImportanceDenomenator);

					double iterationDifference = thisSentanceRank.get(sentance) 
							- previousSentanceRank.get(sentance);
					if (iterationDifference < minIterationDifference) {
						minIterationDifference = iterationDifference;
					}
				}
				iteration += 1;
			}
			return ImmutableMap.<Sentance,Double>copyOf(thisSentanceRank);
		}
}
