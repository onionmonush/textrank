package com.radishfactory.textrank;

import java.util.Set;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import com.google.common.collect.Table;
import com.google.common.collect.Table.Cell;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableTable;
import com.google.common.collect.ImmutableMap;


public class SentanceSimilarities {
	ImmutableTable<Sentance, Sentance, Double> sentanceSimilarityTable;
	ImmutableMap<Sentance, Double> sentanceImportanceOutflows;

	public SentanceSimilarities(List<Sentance> sentances) {
		sentanceSimilarityTable = ImmutableTable.<Sentance,Sentance,Double>builder()
				.putAll(buildNewSentanceSimilarities(sentances))
				.build();
		sentanceImportanceOutflows = ImmutableMap.<Sentance, Double>builder()
				.putAll(buildNewSentanceImportanceOutflows(sentanceSimilarityTable))
				.build();
	}

	public ImmutableTable<Sentance, Sentance, Double> getSentanceSimilarityTable() {
		return sentanceSimilarityTable;
	}

	public ImmutableMap<Sentance, Double> getSentanceImportanceOutflows() {
		return sentanceImportanceOutflows;
	}

	private Table<Sentance, Sentance, Double> buildNewSentanceSimilarities(List<Sentance> sentances) {
		ImmutableTable.Builder<Sentance, Sentance, Double> builder =
				ImmutableTable.<Sentance,Sentance,Double>builder();
		for(int i = 0; i < sentances.size(); i++) {
			for(int j = i + 1; j < sentances.size(); j++) {
				Sentance ithSentance = sentances.get(i);
				Sentance jthSentance = sentances.get(j);

				Double sentanceSimilarity = ithSentance.similarityTo(jthSentance);

				builder.put(ithSentance, jthSentance, sentanceSimilarity );
				builder.put(jthSentance, ithSentance, sentanceSimilarity );
			}
		}
		return builder.build();
	}

	private Map<Sentance, Double> buildNewSentanceImportanceOutflows(
			ImmutableTable<Sentance, Sentance, Double> localSentanceSimilarityTable) {
			HashMap<Sentance, Double> importanceOutflows = new HashMap<Sentance, Double>();

			for(Sentance sentance: localSentanceSimilarityTable.columnKeySet()) {
				importanceOutflows.put(sentance, 0.0);
			}

			for(Cell<Sentance, Sentance, Double> cell : localSentanceSimilarityTable.cellSet()) {
				if (cell.getRowKey() == cell.getColumnKey()) {
					continue;
				}
				importanceOutflows.put(cell.getRowKey(), importanceOutflows.get(cell.getRowKey()) + cell.getValue());
			}

			return importanceOutflows;
	}

}
