package com.radishfactory.textrank;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

public class Sentance {
	String contents;
	List<String> tokens;

	public Sentance(String contents) {
		this.contents = contents.trim();
		this.tokens = tokenize(this.contents);
	}

	public double similarityTo(Sentance otherSentance) {
		Set<String> thisTokenSet = new HashSet<String>(tokens);
		Set<String> otherTokenSet = new HashSet<String>(otherSentance.getTokens());

		thisTokenSet.retainAll(otherTokenSet);
		double similarityStrength = thisTokenSet.size();
		double normalizationFactor = thisTokenSet.size() + otherTokenSet.size();
		return similarityStrength / normalizationFactor;
	}

	public List<String> getTokens() {
		return tokens;
	}

	public String getSentanceText() {
		return contents;
	}
	
	private static List<String> tokenize(String stringToTokenize) {
		ArrayList<String> tokens = new ArrayList<String>();
		for (String token : stringToTokenize.split("\\s+")) { 
			token = token
						.toLowerCase()
						.replace("'s","")
						.replace("s'","")
						.replaceAll("[^a-zA-Z ]", "")
						.trim();
			tokens.add(token);
		}
		return tokens;
	}
}
