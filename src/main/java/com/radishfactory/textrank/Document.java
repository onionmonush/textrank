package com.radishfactory.textrank;

import java.util.ArrayList;
import com.google.common.collect.ImmutableList;

public class Document {
	ImmutableList <Sentance> sentances;

	public Document(String documentText) {
		sentances = parseTextToSentances(documentText);
	}

	public int numSentances() {
		return sentances.size();
	}

	public ImmutableList<Sentance> getSentances() {
		return sentances;
	}

	public Sentance getNthSentance(int n) {
		return sentances.get(n);
	}

	private ImmutableList<Sentance> parseTextToSentances(String documentText) {
		ImmutableList.Builder<Sentance> sentanceBuilder = ImmutableList.<Sentance>builder(); 
		for(String sentance : documentText.split("(?<=[.!?])")) {
			sentanceBuilder.add(new Sentance(sentance));
		}
		return sentanceBuilder.build();
	}

}
